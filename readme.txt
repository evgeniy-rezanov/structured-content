== Structured Content (JSON-LD) #wpsc ==

Contributors: gorbo,antonioleutsch
Tags: json-ld, FAQPage, JobPosting, Event, wpsc, structured content, serps, micro data, structured data
Donate link: https://paypal.me/antonioleutsch
Requires at least: 5.0
Tested up to: 5.2.1
Requires PHP: 7.0
Stable tag: 1.2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add flexible content boxes with JSON-LD microdata output according to schema.org e.g. FAQ or JobPosting. It's your chance to beat the competition and for higher rank results – SEO for winners. Gutenberg ready! #wpsc

== Installation ==

1. Unzip the download package
2. Upload structured-content to the /wp-content/plugins/ directory
3. Activate the plugin through the \'Plugins\' menu in WordPress

Alternatively
1. upload the zip file from the Admin plugins page
2. then activate

== What does it do ==
With this plugin you can insert structured data elements multiple times in any post or page. In simple dialogs, for example FAQ can be inserted. Because the the plugin renders the given information as JSON-LD according to schema.org, the bots of the search engines, like google, recognize this schema. 
Nice option: you can decide if only the JSON-LD should be displayed in the source code or if the content should be preformatted and visible.

You can test whether the information has been labeled correctly with the "Structured Data Testing Tool" from Google (https://search.google.com/structured-data/testing-tool), for example.
The basic goal is that your content can be better understood by the algorithms of different search engines.
A special goal is, among other things, that your answers to questions from Google are displayed as featured snippets in the SERPs (position 0).

The plugin works fine with the TinyMCE and Gutenberg! Currently the plugin offers four structured data elements:
- FAQ
- JobPosting
- Event
- Person

Structure your content now and MAKE CONTENT GREAT AGAIN! #wpsc

== How to use it ==
Once the the plugin is installed and activated, you'll find a new icon in the titlebar of the WYSIWYG editor. Just click it, select your preferred structured content element you want to insert and a modal will open. Fill out the form, click the save button and your done.
If you use "Gutenberg" you'll find a new content block. Choose your preferred structured content element and fill out the form.

Alternatively use these shortcodes in your TinyMCE: 

**FAQ**
[sc_fs_faq html="true" headline="p or h2-h6" img="img-id-231" question="your question" img_alt="img-alt text" css_class="your-class"]your answer – you can format it as you want[/sc_fs_faq]

**JobPosting**
[sc_fs_job html="true" title="JobPosting Title" title_tag="h3" description="JobPosting Description" valid_through="2019-11-08" employment_type="FULL_TIME" company_name="Your Company" same_as="https://gorbo.de" logo_id="309" street_address="anystreet 4" address_locality="Any City" address_region="ST" postal_code="01234" address_country="US" currency_code="USD" quantitative_value="200" base_salary="HOUR" css_class="" ]

**Event**
[sc_fs_event html="true" title="Event tiele" title_tag="h3" event_location="Your Headquarter" start_date="2019-08-29" end_date="2019-06-30" street_address="Any Street 4" address_locality="Any City" address_region="ST" postal_code="01234" address_country="US" css_class="" ]Event-Description – you can format it as you want[/sc_fs_event]

== Updates ==
We will continuously offer new structured data elements and deliver them as updates. Please visit https://gitlab.com/webwirtschaft/structured-content to get the latest information.

== Screenshots ==
1. Adding a FAQ section.
2. Adding a JobPosting
3. Adding a Event

== Sponsoring ==
If you want a special structured data element, we can implement it especially for your needs. As a sponsor you will be mentioned on the website, the plugin description and the changelog. If you are interested, write us an e-mail. 

== Credits ==
Thanks for your support: codemacher, web/dev/media, pixeldreher, superguppi

== Changelog ==

Please visit https://gitlab.com/webwirtschaft/structured-content/activity to see detailed changes.

== Frequently Asked Questions ==

= Where i can report bugs? =

Please use the "Issue" section of the gitlab page of the Plugin: https://gitlab.com/webwirtschaft/structured-content/issues
