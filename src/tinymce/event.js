export default function (editor) {
    return {
        text   : 'Event',
        tooltip: 'Adds a Event block to the page.',
        onclick: () => {
            editor.windowManager.open({
                title     : 'Featured Snippet Event',
                minWidth  : 500,
                height    : 500,
                autoScroll: true,
                classes   : 'sc-panel',
                body      : [
                    {
                        type   : 'checkbox',
                        name   : 'giveHTML',
                        label  : 'Render HTML',
                        checked: true
                    },
                    {
                        type  : 'listbox',
                        name  : 'titleTag',
                        label : 'Headline-Tag',
                        values: [
                            {text: 'h2', value: 'h2'},
                            {text: 'h3', value: 'h3'},
                            {text: 'h4', value: 'h4'},
                            {text: 'h5', value: 'h5'},
                            {text: 'h6', value: 'h6'},
                            {text: 'p', value: 'p'},
                        ],
                        value : 'h2', // Sets the default
                    },
                    {
                        label      : 'Job Title',
                        type       : 'textbox',
                        name       : 'title',
                        value      : '',
                        placeholder: 'Enter Your Event Title...',
                        multiline  : true,
                    },
                    {
                        type       : 'textbox',
                        name       : 'description',
                        label      : 'Description',
                        value      : '',
                        placeholder: 'Enter your Event Description here...',
                        multiline  : true,
                        minHeight  : 100,
                    },
                    {
                        type : 'container',
                        name : 'container',
                        label: '',
                        html : '<h1 style="font-weight: bold;">Company</h1>'
                    },
                    {
                        type       : 'textbox',
                        name       : 'eventLocation',
                        label      : 'Name',
                        value      : '',
                        placeholder: 'Event Location Name',
                        multiline  : true,
                    },
                    {
                        type   : 'textbox',
                        name   : 'startDate',
                        label  : 'Start Date',
                        classes: 'sc_start_date',
                    },
                    {
                        type   : 'textbox',
                        name   : 'endDate',
                        label  : 'End Date',
                        classes: 'sc_end_date',
                    },
                    {
                        type : 'container',
                        name : 'container',
                        label: '',
                        html : '<h1 style="font-weight: bold;">Event Location</h1>'
                    },
                    {
                        type       : 'textbox',
                        name       : 'streetAddress',
                        label      : 'Street',
                        value      : '',
                        placeholder: 'Any Street 3A',
                    },
                    {
                        type       : 'textbox',
                        name       : 'postalCode',
                        label      : 'Postal Code',
                        value      : '',
                        placeholder: 'Any Postal Code',
                    },
                    {
                        type       : 'textbox',
                        name       : 'addressLocality',
                        label      : 'Locality',
                        value      : '',
                        placeholder: 'Any City',
                    },
                    {
                        type       : 'textbox',
                        name       : 'addressCountry',
                        label      : 'Country ISO Code',
                        value      : '',
                        placeholder: 'US',
                    },
                    {
                        type       : 'textbox',
                        name       : 'addressRegion',
                        label      : 'Region ISO Code',
                        value      : '',
                        placeholder: 'CA',
                    },
                    {
                        type : 'textbox',
                        name : 'cssClass',
                        label: 'CSS classes',
                        value: '',
                    },
                ],
                onsubmit  : e => {
                    editor.insertContent(
                        `[sc_fs_event 
                html="${e.data.giveHTML}" 
                title="${e.data.title}" 
                title_tag="${e.data.titleTag}"
                event_location="${e.data.eventLocation}"
                start_date="${e.data.startDate}"
                end_date="${e.data.endDate}"
                street_address="${e.data.streetAddress}"
                address_locality="${e.data.addressLocality}"
                address_region="${e.data.addressRegion}"
                postal_code="${e.data.postalCode}"
                address_country="${e.data.addressCountry}"
                css_class="${e.data.cssClass}"
                ]
                ${e.data.description}
                [/sc_fs_event]`
                    );
                },
            });
            $('.sc_end_date').prop('type', 'date');
            $('.sc_start_date').prop('type', 'date');
        },
    }
};
