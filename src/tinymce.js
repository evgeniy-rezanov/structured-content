import faq from './tinymce/faq';
import job from './tinymce/job';
import event from './tinymce/event';
import person from './tinymce/person';

(function () {
    if (typeof tinymce !== 'undefined') {
        tinymce.PluginManager.add('structured_content_dropdown', function (
            editor,
            url
        ) {
            editor.addButton('structured_content_dropdown', {
                icon: 'structured-content',
                type: 'menubutton',
                menu: [
                    faq(editor),
                    job(editor),
                    event(editor),
                    person(editor)
                ],
            });
        });
    }
})();

jQuery(document).ready(function ($) {
    $(document).on('click', '.mce-select_image', upload_image_tinymce);

    function upload_image_tinymce(e) {
        e.preventDefault();
        const $input_field = $('.mce-image');
        const custom_uploader = (wp.media.frames.file_frame = wp.media({
            title   : 'Add Image',
            button  : {
                text: 'Add Image',
            },
            multiple: false,
        }));
        custom_uploader.on('select', function () {
            const attachment = custom_uploader
                .state()
                .get('selection')
                .first()
                .toJSON();
            $input_field.val(attachment.id);
        });
        custom_uploader.open();
    }
});
