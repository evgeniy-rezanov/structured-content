/**
 * External dependencies
 */
import pick from 'lodash/pick';
import get from 'lodash/get';

// Set dim ratio.
export function overlayToClass( ratio ) {
	return ( ratio === 0 || ratio === 50 ) ?
		null :
		'has-background-overlay-' + ( 10 * Math.round( ratio / 10 ) );
}

// Pick image media attributes.
export const pickRelevantMediaFiles = ( image ) => {
	const imageProps = pick( image, [ 'alt', 'id', 'link', 'caption' ] );
	imageProps.url = get( image, [ 'sizes', 'large', 'url' ] ) || get( image, [ 'media_details', 'sizes', 'large', 'source_url' ] ) || image.url;
	return imageProps;
};

export const escapeQuotes = (string) => {
    return string.replace(/"/g, '&quot;');
};

export const escapeQuotesSingle = (string) => {
	console.log("convertert");
    return string.replace(/"/g, '\"');
};

// Define accepted media for gallery blocks.
export const ALLOWED_GALLERY_MEDIA_TYPES = [ 'image' ];