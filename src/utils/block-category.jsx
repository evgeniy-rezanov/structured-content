/**
 * WordPress dependencies
 */
const { getCategories, setCategories } = wp.blocks;

/**
 * Internal dependencies
 */
import brandAssets from './brand-assets.jsx';

setCategories( [
	// Add a StructuredContent block category
	{
		slug: 'structured-content',
		title: 'Structured Content',
		icon: brandAssets.categoryIcon,
	},
	...getCategories().filter( ( { slug } ) => slug !== 'structured-content' ),
] );