/**
 * Internal dependencies
 */
import InfoLabel from '../../components/infolabel'
import icons from '../../utils/icons.jsx';
import {escapeQuotes, escapeQuotesSingle} from '../../utils/helper.jsx';

/**
 * WordPress dependencies
 */
const {__} = wp.i18n; // Import __() from wp.i18n
const {RichText, PlainText, AlignmentToolbar, MediaUpload, InspectorControls, BlockControls} = wp.editor;
const {Fragment} = wp.element;
const {PanelRow, PanelBody, SelectControl, TextControl, ToggleControl} = wp.components;
const {registerBlockType} = wp.blocks;

/**
 * Block constants
 */
const name = 'job';

const title = __('Job', 'structured-content');

const icon = icons.job;

const iconColor = '#F03009';

const keywords = [
    __('job search', 'structured-content'),
    __('job offer', 'structured-content'),
    __('structured-content', 'structured-content'),
];


const blockAttributes = {
    jobTitle         : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__title',
        default : 'Job Title'
    },
    titleTag         : {
        type   : 'string',
        default: 'h2',
    },
    description      : {
        type    : 'html',
        selector: '.wp-block-structured-content-job__description',
        default : '',
    },
    validThrough     : {
        type   : 'string',
        default: '',
    },
    companyName      : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__companyName',
        default : '',
    },
    employmentType   : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__type',
        default : 'FULL_TIME',
    },
    sameAs           : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__sameAs',
        default : '',
    },
    streetAddress    : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__streetAddress',
        default : '',
    },
    addressLocality  : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__addressLocality',
        default : '',
    },
    postalCode       : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__postalCode',
        default : '',
    },
    addressRegion    : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__addressRegion',
        default : '',
    },
    addressCountry   : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__addressCountry',
        default : '',
    },
    baseSalary       : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__baseSalary',
        default : 'HOUR'
    },
    currencyCode     : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__currency',
        default : '',
    },
    quantitativeValue: {
        type    : 'string',
        selector: '.wp-block-structured-content-job__quantitativeValue',
        default : '',
    },
    imageID          : {
        type   : 'number',
        default: ''
    },
    imageAlt         : {
        type   : 'string',
        default: ''
    },
    thumbnailImageUrl: {
        type   : 'string',
        default: ''
    },
    cssClass         : {
        type   : 'string',
        default: ''
    },
    textAlign        : {
        type: 'string',
    },
    giveHTML         : {
        type   : 'bool',
        default: true,
    },
};


/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType(`structured-content/${name}`, {
    title   : title, // Block title.
    icon    : {src: icon, foreground: iconColor},
    category: 'structured-content',
    keywords: keywords,

    attributes: blockAttributes,

    supports: {
        align          : ['wide', 'full'],
        stackedOnMobile: true,
    },

    edit: ({
               attributes,
               className,
               isSelected,
               setAttributes,
           }) => {

        const {
                  align,
                  textAlign,
                  titleTag,
                  jobTitle,
                  cssClass,
                  description,
                  sameAs,
                  companyName,
                  streetAddress,
                  addressLocality,
                  postalCode,
                  addressRegion,
                  addressCountry,
                  baseSalary,
                  currencyCode,
                  quantitativeValue,
                  validThrough,
                  employmentType,
                  imageID,
                  imageAlt,
                  thumbnailImageUrl,
                  giveHTML,
              } = attributes;

        function onImageSelect(imageObject) {
            console.log(imageObject);
            setAttributes({
                imageID          : imageObject.id,
                imageAlt         : imageObject.alt,
                thumbnailImageUrl: imageObject.sizes.thumbnail.url,
            });
        }

        function onRemoveImage() {
            setAttributes({
                imageID          : null,
                imageAlt         : null,
                thumbnailImageUrl: '',
            });
        }


        return [
            <Fragment>
                {isSelected && (
                    <Fragment>
                        <BlockControls>
                            <AlignmentToolbar
                                value={textAlign}
                                onChange={(nextTextAlign) => setAttributes({textAlign: nextTextAlign})}
                            />
                        </BlockControls>
                    </Fragment>
                )}
                {isSelected && (
                    <Fragment>
                        <InspectorControls>
                            <PanelBody>
                                <PanelRow>
                                    <SelectControl
                                        label={__('Title tag', 'structured-content')}
                                        value={titleTag}
                                        className="w-100"
                                        options={[
                                            {label: 'H2', value: 'h2'},
                                            {label: 'H3', value: 'h3'},
                                            {label: 'H4', value: 'h4'},
                                            {label: 'H5', value: 'h5'},
                                            {label: 'p', value: 'p'},
                                        ]}
                                        onChange={(titleTag) => {
                                            setAttributes({titleTag: titleTag})
                                        }}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                        label={__('CSS class', 'structured-content')}
                                        className="w-100"
                                        value={cssClass}
                                        placeholder={__('additional css classes ...', 'structured-content')}
                                        onChange={(cssClass) => setAttributes({cssClass: cssClass})}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ToggleControl
                                        label={__('Render HTML', 'structured-content')}
                                        checked={giveHTML}
                                        onChange={(giveHTML) => {
                                            setAttributes({giveHTML: giveHTML})
                                        }}/>
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    </Fragment>
                )}
                <section
                    className={
                        className,
                        align && `align${align}`,
                        cssClass ? cssClass : `sc_card`
                    }
                    style={{
                        textAlign: textAlign,
                    }}>
                    <div style={{marginBottom: 5, overflow: 'hidden'}}>
                        {InfoLabel({
                            URL  : 'https://developers.google.com/search/docs/data-types/job-posting',
                            float: 'right'
                        })}
                    </div>
                    <div>
                        {wp.element.createElement(titleTag, {},
                            <PlainText
                                placeholder={__('Enter Your job title here...', 'structured-content')}
                                value={jobTitle}
                                className='wp-block-structured-content-job__title'
                                tag={titleTag}
                                onChange={(value) => setAttributes({jobTitle: value})}
                                keepPlaceholderOnFocus
                            />
                        )}
                        <div>
                            <div className="answer" itemProp="text">
                                <RichText
                                    placeholder={__('Enter your job description here...', 'structured-content')}
                                    value={description}
                                    className='wp-block-structured-content-job__text'
                                    onChange={(value) => setAttributes({description: value})}
                                    keepPlaceholderOnFocus
                                />
                            </div>
                        </div>
                        <div className="sc-row" style={{marginTop: 15}}>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Company', 'structured-content')}
                                </div>
                                <div style={{display: 'grid', gridTemplateColumns: '2fr 1fr', gridColumnGap: 15}}>
                                    <div className="sc-company-infos">

                                        <div className="sc-input-group">
                                            <div className="sc-input-label">
                                                {__('Name', 'structured-content')}
                                            </div>
                                            <TextControl
                                                type="text"
                                                value={companyName}
                                                placeholder="Company Name"
                                                className="wp-block-structured-content-job__companyName"
                                                onChange={(companyName) => setAttributes({companyName: companyName})}
                                            />
                                        </div>
                                        <div className="sc-input-group">
                                            <div className="sc-input-label">
                                                {__('Same as (Website / Social Media)', 'structured-content')}
                                            </div>
                                            <TextControl
                                                type="url"
                                                value={sameAs}
                                                placeholder={__('https://your-website.com', 'structured-content')}
                                                className="wp-block-structured-content-job__sameAs"
                                                onChange={(sameAs) => setAttributes({sameAs: sameAs})}
                                            />
                                        </div>
                                    </div>
                                    <div className="sc-company-logo">
                                        <div className="sc-input-group">
                                            <div className="sc-input-label">
                                                {__('Logo', 'structured-content')}
                                            </div>
                                            <div>
                                                <MediaUpload
                                                    onSelect={onImageSelect}
                                                    type="image"
                                                    value={imageID}
                                                    render={({open}) => (
                                                        <button className="components-button is-button is-default"
                                                                onClick={open}>
                                                            {__('Add Image', 'structured-content')}
                                                        </button>
                                                    )}
                                                />
                                                {thumbnailImageUrl && (
                                                    <div>
                                                        <div style={{
                                                            'height'       : 'auto',
                                                            'marginTop'    : 15,
                                                            'background'   : 'rgb(255, 255, 255)',
                                                            'boxShadow'    : 'rgba(0, 0, 0, 0.16) 0px 2px 2px, rgba(0, 0, 0, 0.08) 0px 0px 1px',
                                                            'borderRadius' : '4px',
                                                            'maxWidth'     : '100%',
                                                            'width'        : 'auto',
                                                            'overflow'     : 'hidden',
                                                            'margin-bottom': 10
                                                        }}>
                                                            <img src={thumbnailImageUrl} style={{
                                                                'width'    : 'auto',
                                                                'height'   : 'auto',
                                                                'max-width': '100%',
                                                                'margin'   : 0
                                                            }}/>
                                                        </div>
                                                        <button className="components-button is-button is-default"
                                                                onClick={() => onRemoveImage()}>
                                                            {__('Remove Image', 'structured-content')}
                                                        </button>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Job Location', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Street', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('Any Street 3A', 'structured-content')}
                                        type="text"
                                        value={streetAddress}
                                        className="wp-block-structured-content-job__streetAddress"
                                        onChange={(streetAddress) => setAttributes({streetAddress: streetAddress})}
                                    />
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Postal Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('Any Postal Code', 'structured-content')}
                                            type="text"
                                            value={postalCode}
                                            className="wp-block-structured-content-job__postalCode"
                                            onChange={(postalCode) => setAttributes({postalCode: postalCode})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Locality', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('Any City', 'structured-content')}
                                            type="text"
                                            value={addressLocality}
                                            className="wp-block-structured-content-job__addressLocality"
                                            onChange={(addressLocality) => setAttributes({addressLocality: addressLocality})}
                                        />
                                    </div>
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Country ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('US', 'structured-content')}
                                            type="text"
                                            value={addressCountry}
                                            className="wp-block-structured-content-job__addressCountry"
                                            onChange={(addressCountry) => setAttributes({addressCountry: addressCountry})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Region ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('CA', 'structured-content')}
                                            type="text"
                                            value={addressRegion}
                                            className="wp-block-structured-content-job__addressRegion"
                                            onChange={(addressRegion) => setAttributes({addressRegion: addressRegion})}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sc-row" style={{marginTop: 15}}>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Salary', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Unit', 'structured-content')}
                                    </div>
                                    <SelectControl
                                        value={baseSalary}
                                        className="w-100"
                                        options={[
                                            {label: __('Hourly', 'structured-content'), value: 'HOUR'},
                                            {label: __('Daily', 'structured-content'), value: 'DAY'},
                                            {label: __('Weekly', 'structured-content'), value: 'WEEK'},
                                            {label: __('Monthly', 'structured-content'), value: 'MONTH'},
                                            {label: __('Yearly', 'structured-content'), value: 'YEAR'},
                                        ]}
                                        onChange={(baseSalary) => {
                                            setAttributes({baseSalary: baseSalary})
                                        }}
                                    />
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Currency ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('USD', 'structured-content')}
                                            value={currencyCode}
                                            type="text"
                                            className='wp-block-structured-content-job__currency'
                                            onChange={(currencyCode) => setAttributes({currencyCode: currencyCode})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Value', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder="40"
                                            type="number"
                                            value={quantitativeValue}
                                            className="wp-block-structured-content-job__quantitativeValue"
                                            onChange={(quantitativeValue) => setAttributes({quantitativeValue: quantitativeValue})}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Job Meta', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Employment Type', 'structured-content')}
                                    </div>
                                    <SelectControl
                                        value={employmentType}
                                        className="w-100"
                                        options={[
                                            {label: __('Full Time', 'structured-content'), value: 'FULL_TIME'},
                                            {label: __('Part Time', 'structured-content'), value: 'PART_TIME'},
                                            {label: __('Contractor', 'structured-content'), value: 'CONTRACTOR'},
                                            {label: __('Temporary', 'structured-content'), value: 'TEMPORARY'},
                                            {label: __('Intern', 'structured-content'), value: 'INTERN'},
                                            {label: __('Volunteer', 'structured-content'), value: 'VOLUNTEER'},
                                            {label: __('Per Diem', 'structured-content'), value: 'PER_DIEM'},
                                            {label: __('Other', 'structured-content'), value: 'OTHER'},
                                        ]}
                                        onChange={(employmentType) => {
                                            setAttributes({employmentType: employmentType});
                                        }}
                                    />
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Valid Through', 'structured-content')}
                                    </div>
                                    <TextControl
                                        type="date"
                                        value={validThrough}
                                        onChange={(validThrough) => setAttributes({validThrough: validThrough})}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        ];
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: function (props) {

        const {attributes, className} = props;
        const {thumbnailImageUrl} = props.attributes;

        console.log(attributes, className, thumbnailImageUrl);
        console.log(
            `[sc_fs_job 
            html="${attributes.giveHTML}" 
            title="${attributes.jobTitle}" 
            title_tag="${attributes.titleTag}" 
            description="${escapeQuotesSingle(attributes.description)}" 
            valid_through="${attributes.validThrough}" 
            employment_type="${attributes.employmentType}" 
            company_name="${attributes.companyName}" 
            same_as="${attributes.sameAs}" 
            logo_id="${attributes.imageID}"
            street_address="${attributes.streetAddress}"
            address_locality="${attributes.addressLocality}"
            address_region="${attributes.addressRegion}"
            postal_code="${attributes.postalCode}"
            address_country="${attributes.addressCountry}"
            currency_code="${attributes.currencyCode}"
            quantitative_value="${attributes.quantitativeValue}"
            base_salary="${attributes.baseSalary}"
            css_class="${attributes.cssClass}"
            ]`
        );
        return (
            `[sc_fs_job 
            html="${attributes.giveHTML}" 
            title="${attributes.jobTitle}" 
            title_tag="${attributes.titleTag}"
            valid_through="${attributes.validThrough}" 
            employment_type="${attributes.employmentType}" 
            company_name="${attributes.companyName}" 
            same_as="${attributes.sameAs}" 
            logo_id="${attributes.imageID}"
            street_address="${attributes.streetAddress}"
            address_locality="${attributes.addressLocality}"
            address_region="${attributes.addressRegion}"
            postal_code="${attributes.postalCode}"
            address_country="${attributes.addressCountry}"
            currency_code="${attributes.currencyCode}"
            quantitative_value="${attributes.quantitativeValue}"
            base_salary="${attributes.baseSalary}"
            css_class="${attributes.cssClass}"
            ]${escapeQuotes(attributes.description)}[/sc_fs_job]`
        );
    }
});
