/**
 * Internal dependencies
 */
import InfoLabel from '../../components/infolabel'
import icons from '../../utils/icons.jsx';
import {escapeQuotes} from '../../utils/helper.jsx';

/**
 * WordPress dependencies
 */

const {__} = wp.i18n; // Import __() from wp.i18n
const {Fragment} = wp.element;
const {RichText, PlainText, AlignmentToolbar, MediaUpload, InspectorControls, BlockControls} = wp.editor;
const {PanelRow, PanelBody, SelectControl, TextControl, ToggleControl} = wp.components;
const {registerBlockType} = wp.blocks;

/**
 * Block constants
 */
const name = 'event';

const title = __('Event', 'structured-content');

const icon = icons.event;

// Custom foreground icon color based on the StructuredContent branding
const iconColor = '#F03009';

const keywords = [
    __('event', 'structured-content'),
    __('structured-content', 'structured-content'),
];


const blockAttributes = {
    title          : {
        type    : 'string',
        selector: '.wp-block-structured-content-event__title',
    },
    titleTag       : {
        type   : 'string',
        default: 'h2'
    },
    description    : {
        type    : 'string',
        selector: '.wp-block-structured-content-event__description',
        default : '',
    },
    eventLocation  : {
        type    : 'string',
        selector: '.wp-block-structured-content-event__location',
    },
    startDate      : {
        type   : 'string',
        default: '',
    },
    endDate        : {
        type   : 'string',
        default: '',
    },
    streetAddress  : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__streetAddress',
        default : '',
    },
    addressLocality: {
        type    : 'string',
        selector: '.wp-block-structured-content-job__addressLocality',
        default : '',
    },
    postalCode     : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__postalCode',
        default : '',
    },
    addressRegion  : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__addressRegion',
        default : '',
    },
    addressCountry : {
        type    : 'string',
        selector: '.wp-block-structured-content-job__addressCountry',
        default : '',
    },
    cssClass       : {
        type   : 'string',
        default: ''
    },
    textAlign      : {
        type: 'string',
    },
    giveHTML       : {
        type   : 'bool',
        default: true,
    },
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType(`structured-content/${name}`, {
    title   : title, // Block title.
    icon    : {src: icon, foreground: iconColor},
    category: 'structured-content',
    keywords: keywords,

    attributes: blockAttributes,

    supports: {
        align          : ['wide', 'full'],
        stackedOnMobile: true,
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit: ({
               attributes,
               className,
               isSelected,
               setAttributes,
           }) => {

        const {
                  align,
                  textAlign,
                  title,
                  titleTag,
                  description,
                  cssClass,
                  giveHTML,
                  eventLocation,
                  startDate,
                  endDate,
                  streetAddress,
                  addressLocality,
                  postalCode,
                  addressRegion,
                  addressCountry,
              } = attributes;

        return [
            <Fragment>
                {isSelected && (
                    <Fragment>
                        <BlockControls>
                            <AlignmentToolbar
                                value={textAlign}
                                onChange={(nextTextAlign) => setAttributes({textAlign: nextTextAlign})}
                            />
                        </BlockControls>
                    </Fragment>
                )}
                {isSelected && (
                    <Fragment>
                        <InspectorControls>
                            <PanelBody>
                                <PanelRow>
                                    <SelectControl
                                        label={__('Title tag', 'structured-content')}
                                        value={titleTag}
                                        className="w-100"
                                        options={[
                                            {label: 'H2', value: 'h2'},
                                            {label: 'H3', value: 'h3'},
                                            {label: 'H4', value: 'h4'},
                                            {label: 'H5', value: 'h5'},
                                            {label: 'p', value: 'p'},
                                        ]}
                                        onChange={(titleTag) => {
                                            setAttributes({titleTag: titleTag})
                                        }}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                        label={__('CSS class', 'structured-content')}
                                        className="w-100"
                                        value={cssClass}
                                        placeholder={__('additional css classes ...', 'structured-content')}
                                        onChange={(cssClass) => setAttributes({cssClass: cssClass})}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ToggleControl
                                        label={__('Render HTML', 'structured-content')}
                                        checked={giveHTML}
                                        onChange={(giveHTML) => {
                                            setAttributes({giveHTML: giveHTML})
                                        }}/>
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    </Fragment>
                )}
                <section
                    className={
                        className,
                        align && `align${align}`,
                        cssClass ? cssClass : `sc_card`
                    }
                    style={{
                        textAlign: textAlign,
                    }}>
                    <div style={{marginBottom: 5, overflow: 'hidden'}}>
                        {InfoLabel({
                            URL  : 'https://developers.google.com/search/docs/data-types/event',
                            float: 'right'
                        })}
                    </div>
                    <div>
                        {wp.element.createElement(titleTag, {class: 'title'},
                            <PlainText
                                placeholder={__('Enter Your Event Title...', 'structured-content')}
                                value={title}
                                className='wp-block-structured-content-event__title title'
                                tag={titleTag}
                                onChange={(value) => setAttributes({title: value})}
                                keepPlaceholderOnFocus
                            />
                        )}
                        <div>
                            <div className="description" itemProp="text">
                                <RichText
                                    placeholder={__('Enter your Event Description here...', 'structured-content')}
                                    value={description}
                                    className='wp-block-structured-content-event__text'
                                    onChange={(value) => setAttributes({description: value})}
                                    keepPlaceholderOnFocus
                                />
                            </div>
                        </div>
                        <div className="sc-row" style={{marginTop: 15}}>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Event Meta', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Name', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('Event Location Name', 'structured-content')}
                                        value={eventLocation}
                                        type="text"
                                        className='wp-block-structured-content-event__location'
                                        onChange={(eventLocation) => setAttributes({eventLocation: eventLocation})}
                                    />
                                </div>

                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Start Date', 'structured-content')}
                                    </div>
                                    <TextControl
                                        type="date"
                                        value={startDate}
                                        onChange={(startDate) => setAttributes({startDate: startDate})}
                                    />
                                </div>

                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('End Date', 'structured-content')}
                                    </div>
                                    <TextControl
                                        type="date"
                                        value={endDate}
                                        onChange={(endDate) => setAttributes({endDate: endDate})}
                                    />
                                </div>
                            </div>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Event Location', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Street', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('Any Street 3A', 'structured-content')}
                                        type="text"
                                        value={streetAddress}
                                        className="wp-block-structured-content-job__streetAddress"
                                        onChange={(streetAddress) => setAttributes({streetAddress: streetAddress})}
                                    />
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Postal Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('Any Postal Code', 'structured-content')}
                                            type="text"
                                            value={postalCode}
                                            className="wp-block-structured-content-job__postalCode"
                                            onChange={(postalCode) => setAttributes({postalCode: postalCode})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Locality', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('Any City', 'structured-content')}
                                            type="text"
                                            value={addressLocality}
                                            className="wp-block-structured-content-job__addressLocality"
                                            onChange={(addressLocality) => setAttributes({addressLocality: addressLocality})}
                                        />
                                    </div>
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Country ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('US', 'structured-content')}
                                            type="text"
                                            value={addressCountry}
                                            className="wp-block-structured-content-job__addressCountry"
                                            onChange={(addressCountry) => setAttributes({addressCountry: addressCountry})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Region ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('CA', 'structured-content')}
                                            type="text"
                                            value={addressRegion}
                                            className="wp-block-structured-content-job__addressRegion"
                                            onChange={(addressRegion) => setAttributes({addressRegion: addressRegion})}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        ];
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: function (props) {

        const {attributes, className} = props;
        const {thumbnailImageUrl} = props.attributes;

        console.log(attributes, className, thumbnailImageUrl, props);
        return (
            `[sc_fs_event 
                html="${attributes.giveHTML}" 
                title="${attributes.title}" 
                title_tag="${attributes.titleTag}"
                event_location="${attributes.eventLocation}"
                start_date="${attributes.startDate}"
                end_date="${attributes.endDate}"
                street_address="${attributes.streetAddress}"
                address_locality="${attributes.addressLocality}"
                address_region="${attributes.addressRegion}"
                postal_code="${attributes.postalCode}"
                address_country="${attributes.addressCountry}"
                css_class="${attributes.cssClass}"
                ]
                ${escapeQuotes(attributes.description)}
                [/sc_fs_event]`
        );
    },
});