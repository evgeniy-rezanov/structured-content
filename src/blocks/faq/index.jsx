/**
 * Internal dependencies
 */
import InfoLabel from '../../components/infolabel'
import icons from '../../utils/icons.jsx';
import {escapeQuotes} from '../../utils/helper.jsx';

/**
 * WordPress dependencies
 */

const {__} = wp.i18n; // Import __() from wp.i18n
const {Fragment} = wp.element;
const {RichText, PlainText, AlignmentToolbar, MediaUpload, InspectorControls, BlockControls} = wp.editor;
const {PanelRow, PanelBody, SelectControl, TextControl, ToggleControl} = wp.components;
const {registerBlockType} = wp.blocks;

/**
 * Block constants
 */
const name = 'faq';

const title = __('FAQ', 'structured-content');

const icon = icons.faq;

// Custom foreground icon color based on the StructuredContent branding
const iconColor = '#F03009';

const keywords = [
    __('faq question', 'structured-content'),
    __('faq answer', 'structured-content'),
    __('structured-content', 'structured-content'),
];


const blockAttributes = {
    question         : {
        type    : 'string',
        selector: '.wp-block-structured-content-faq__title',
    },
    questionTag      : {
        type   : 'string',
        default: 'h2'
    },
    answer           : {
        type    : 'string',
        selector: '.wp-block-structured-content-faq__answer',
        default : '',
    },
    imageID          : {
        type   : 'number',
        default: ''
    },
    imageAlt         : {
        type   : 'string',
        default: ''
    },
    thumbnailImageUrl: {
        type   : 'string',
        default: ''
    },
    cssClass         : {
        type   : 'string',
        default: ''
    },
    textAlign        : {
        type: 'string',
    },
    giveHTML         : {
        type   : 'bool',
        default: true,
    },
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType(`structured-content/${name}`, {
    title   : title, // Block title.
    icon    : {src: icon, foreground: iconColor},
    category: 'structured-content',
    keywords: keywords,

    attributes: blockAttributes,

    supports: {
        align          : ['wide', 'full'],
        stackedOnMobile: true,
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit: ({
               attributes,
               className,
               isSelected,
               setAttributes,
           }) => {

        const {
                  align,
                  textAlign,
                  question,
                  questionTag,
                  answer,
                  cssClass,
                  imageID,
                  imageAlt,
                  thumbnailImageUrl,
                  giveHTML,
              } = attributes;

        function onImageSelect(imageObject) {
            setAttributes({
                imageID          : imageObject.id,
                imageAlt         : imageObject.alt,
                thumbnailImageUrl: imageObject.sizes.thumbnail.url,
            });
        }

        function onRemoveImage() {
            setAttributes({
                imageID          : null,
                imageAlt         : null,
                thumbnailImageUrl: '',
            });
        }


        return [
            <Fragment>
                {isSelected && (
                    <Fragment>
                        <BlockControls>
                            <AlignmentToolbar
                                value={textAlign}
                                onChange={(nextTextAlign) => setAttributes({textAlign: nextTextAlign})}
                            />
                        </BlockControls>
                    </Fragment>
                )}
                {isSelected && (
                    <Fragment>
                        <InspectorControls>
                            <PanelBody>
                                <PanelRow>
                                    <label>{__('Image Settings', 'structured-content')}</label>
                                </PanelRow>
                                <PanelRow>
                                    <MediaUpload
                                        onSelect={onImageSelect}
                                        type="image"
                                        value={imageID}
                                        render={({open, thumbnailImageUrl}) => (
                                            <div>
                                                {thumbnailImageUrl && (
                                                    <figure>
                                                        <a href="#" onClick={open}>
                                                            <img src={thumbnailImageUrl}/>
                                                        </a>
                                                    </figure>
                                                )}
                                                <button className="components-button is-button is-default"
                                                        onClick={open}>
                                                    {__('Add Image', 'structured-content')}
                                                </button>
                                            </div>
                                        )}
                                    />
                                    <button className="components-button is-button is-default"
                                            onClick={() => onRemoveImage()}>
                                        {__('Remove Image', 'structured-content')}
                                    </button>
                                </PanelRow>
                                <PanelRow>
                                    <SelectControl
                                        label={__('Question tag', 'structured-content')}
                                        value={questionTag}
                                        className="w-100"
                                        options={[
                                            {label: 'H2', value: 'h2'},
                                            {label: 'H3', value: 'h3'},
                                            {label: 'H4', value: 'h4'},
                                            {label: 'H5', value: 'h5'},
                                            {label: 'p', value: 'p'},
                                        ]}
                                        onChange={(questionTag) => {
                                            setAttributes({questionTag: questionTag})
                                        }}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                        label={__('Image description', 'structured-content')}
                                        className="w-100"
                                        value={imageAlt}
                                        placeholder={__('Enter an image description here ...', 'structured-content')}
                                        onChange={(imageAlt) => setAttributes({imageAlt: imageAlt})}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <TextControl
                                        label={__('CSS class', 'structured-content')}
                                        className="w-100"
                                        value={cssClass}
                                        placeholder={__('additional css classes ...', 'structured-content')}
                                        onChange={(cssClass) => setAttributes({cssClass: cssClass})}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ToggleControl
                                        label={__('Render HTML', 'structured-content')}
                                        checked={giveHTML}
                                        onChange={(giveHTML) => {
                                            setAttributes({giveHTML: giveHTML})
                                        }}/>
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    </Fragment>
                )}
                <section
                    className={
                        className,
                        align && `align${align}`,
                        cssClass ? cssClass : `sc_card`
                    }
                    style={{
                        textAlign: textAlign,
                    }}>
                    <div style={{marginBottom: 5, overflow: 'hidden'}}>
                        {InfoLabel({
                            URL  : 'https://developers.google.com/search/docs/data-types/qapage',
                            float: 'right'
                        })}
                    </div>
                    <div>
                        {wp.element.createElement(questionTag, {class: 'question'},
                            <PlainText
                                placeholder={__('Enter Your Question here...', 'structured-content')}
                                value={question}
                                className='wp-block-structured-content-faq__title question'
                                tag={questionTag}
                                onChange={(value) => setAttributes({question: value})}
                                keepPlaceholderOnFocus
                            />
                        )}
                        <div>
                            <figure>
                                <a href="#" title={attributes.imageAlt}>
                                    <img itemProp="image" src={attributes.thumbnailImageUrl} alt={attributes.imageAlt}/>
                                </a>
                            </figure>
                            <div className="answer" itemProp="text">
                                <RichText
                                    placeholder={__('Enter your answer here...', 'structured-content')}
                                    value={answer}
                                    className='wp-block-structured-content-faq__text'
                                    onChange={(value) => setAttributes({answer: value})}
                                    keepPlaceholderOnFocus
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        ];
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: function (props) {

        const {attributes, className} = props;
        const {thumbnailImageUrl} = props.attributes;

        console.log(attributes, className, thumbnailImageUrl, props);
        return (
            `[sc_fs_faq html="${attributes.giveHTML}" headline="${attributes.questionTag}" question="${attributes.question}" img="${attributes.imageID}" img_alt="${attributes.imageAlt}" css_class="${attributes.cssClass}"]${escapeQuotes(attributes.answer)}[/sc_fs_faq]`
        );
    },
});