/**
 * Internal dependencies
 */
import InfoLabel from '../../components/infolabel'
import icons from '../../utils/icons.jsx';
import {escapeQuotes} from '../../utils/helper.jsx';

/**
 * WordPress dependencies
 */
const {__} = wp.i18n; // Import __() from wp.i18n
const {RichText, PlainText, AlignmentToolbar, MediaUpload, InspectorControls, BlockControls} = wp.editor;
const {Fragment} = wp.element;
const {PanelRow, PanelBody, SelectControl, TextControl, ToggleControl} = wp.components;
const {registerBlockType} = wp.blocks;

import _ from 'lodash';


/**
 * Block constants
 */
const name = 'person';

const title = __('Person', 'structured-content');

const icon = icons.person;

const iconColor = '#F03009';

const keywords = [
    __('person search', 'structured-content'),
    __('person offer', 'structured-content'),
    __('structured-content', 'structured-content'),
];


const blockAttributes = {
    personName       : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__personName',
        default : '',
    },
    jobTitle         : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__jobTitle',
        default : '',
    },
    streetAddress    : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__streetAddress',
        default : '',
    },
    addressLocality  : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__addressLocality',
        default : '',
    },
    postalCode       : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__postalCode',
        default : '',
    },
    addressRegion    : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__addressRegion',
        default : '',
    },
    addressCountry   : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__addressCountry',
        default : '',
    },
    email            : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__email',
        default : '',
    },
    homepage         : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__homepage',
        default : '',
    },
    telephone        : {
        type    : 'string',
        selector: '.wp-block-structured-content-person__telephone',
        default : '',
    },
    imageID          : {
        type   : 'number',
        default: ''
    },
    imageAlt         : {
        type   : 'string',
        default: ''
    },
    thumbnailImageUrl: {
        type   : 'string',
        default: ''
    },
    colleagues       : {
        type   : 'array',
        default: [],
    },
    cssClass         : {
        type   : 'string',
        default: ''
    },
    textAlign        : {
        type: 'string',
    },
    giveHTML         : {
        type   : 'bool',
        default: true,
    },
};


/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType(`structured-content/${name}`, {
    title   : title, // Block title.
    icon    : {src: icon, foreground: iconColor},
    category: 'structured-content',
    keywords: keywords,

    attributes: blockAttributes,

    supports: {
        align          : ['wide', 'full'],
        stackedOnMobile: true,
    },

    edit: ({
               attributes,
               className,
               isSelected,
               setAttributes,
           }) => {

        const {
                  align,
                  textAlign,
                  cssClass,
                  jobTitle,
                  personName,
                  streetAddress,
                  addressLocality,
                  postalCode,
                  addressRegion,
                  addressCountry,
                  email,
                  homepage,
                  telephone,
                  colleagues,
                  imageID,
                  imageAlt,
                  thumbnailImageUrl,
                  giveHTML,
              } = attributes;

        console.log(colleagues);

        function onImageSelect(imageObject) {
            console.log(imageObject);
            setAttributes({
                imageID          : imageObject.id,
                imageAlt         : imageObject.alt,
                thumbnailImageUrl: imageObject.sizes.thumbnail.url,
            });
        }

        function onRemoveImage() {
            setAttributes({
                imageID          : null,
                imageAlt         : null,
                thumbnailImageUrl: '',
            });
        }

        function findNexColleagueID() {
            return Math.max(...colleagues.map(user => user.id)) !== -Infinity ? Math.max(...colleagues.map(user => user.id)) + 1 : 0;
        }

        function addToColleague() {
            let id = findNexColleagueID();
            setAttributes({
                colleagues: [
                    ...colleagues,
                    {id: id, url: ''}
                ]
            });
        }

        function removeFromColleague(ident) {
            let c = _.filter(colleagues, function (f) {
                return f.id !== ident;
            });
            setAttributes({colleagues: c});
        }

        function updateColleague(ident, value) {
            let index = _.findIndex(colleagues, {id: ident});
            colleagues.splice(index, 1, {id: ident, url: value});
            setAttributes({colleagues: colleagues});
        }

        let colleaguesUrls =
                colleagues
                    .sort(function (a, b) {
                        return a.index - b.index;
                    }).map((data, index) => {
                    console.log(colleagues);
                    return (
                        <div style={{
                            display            : 'grid',
                            gridTemplateColumns: '3fr auto',
                            gridColumnGap      : 5,
                            lineHeight         : 1,
                        }}
                             key={`url-${index}`}
                        >
                            <TextControl
                                type="text"
                                value={data.url}
                                placeholder={data.url !== '' ? data.url : __('http://www.xyz.edu/students/alicejones.html', 'structured-content')}
                                keepplaceholderonfocus
                                className={`wp-block-structured-content-person__repeater-${data.id}`}
                                onChange={(value) => (setAttributes(
                                    colleagues[index] = {'id': data.id, 'url': value}
                                ))}
                            />
                            <div>
                                <button type="button" style={{
                                    backgroundColor: 'black',
                                    color          : 'white',
                                    borderRadius   : 4,
                                    padding        : 8,
                                    fontSize       : 12,
                                    border         : 'none',
                                    paddingLeft    : 8,
                                    position       : 'relative',
                                    cursor         : 'pointer'
                                }} onClick={() => removeFromColleague(data.id)}>
                            <span style={{
                                display: 'inline-block',
                            }}>
                                 <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 10" style={{
                                     width   : 12,
                                     height  : 12,
                                     position: 'relative',
                                     top     : 1,
                                 }}>
                                  <circle cx="5" cy="5" r="5" fill="#fff"/>
                                  <path fill="#000" d="M5.7 5.6h2V4.4H2.4v1.2h3.4z"/>
                                </svg>
                            </span>
                                </button>
                            </div>
                        </div>
                    )
                });


        return [
            <Fragment>
                {isSelected && (
                    <Fragment>
                        <BlockControls>
                            <AlignmentToolbar
                                value={textAlign}
                                onChange={(nextTextAlign) => setAttributes({textAlign: nextTextAlign})}
                            />
                        </BlockControls>
                    </Fragment>
                )}
                {isSelected && (
                    <Fragment>
                        <InspectorControls>
                            <PanelBody>
                                <PanelRow>
                                    <TextControl
                                        label={__('CSS class', 'structured-content')}
                                        className="w-100"
                                        value={cssClass}
                                        placeholder={__('additional css classes ...', 'structured-content')}
                                        onChange={(cssClass) => setAttributes({cssClass: cssClass})}
                                    />
                                </PanelRow>
                                <PanelRow>
                                    <ToggleControl
                                        label={__('Render HTML', 'structured-content')}
                                        checked={giveHTML}
                                        onChange={(giveHTML) => {
                                            setAttributes({giveHTML: giveHTML})
                                        }}/>
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    </Fragment>
                )}
                <section
                    className={
                        className,
                        align && `align${align}`,
                        cssClass ? cssClass : `sc_card`
                    }
                    style={{
                        textAlign: textAlign,
                    }}>
                    <div style={{marginBottom: 5, overflow: 'hidden'}}>
                        {InfoLabel({
                            URL  : 'https://developers.google.com/search/docs/data-types/person-posting',
                            float: 'right'
                        })}
                    </div>
                    <div>
                        <div className="sc-row" style={{marginTop: 15}}>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Personal', 'structured-content')}
                                </div>
                                <div style={{display: 'grid', gridTemplateColumns: '2fr 1fr', gridColumnGap: 15}}>
                                    <div className="sc-person-infos">
                                        <div className="sc-input-group">
                                            <div className="sc-input-label">
                                                {__('Name', 'structured-content')}
                                            </div>
                                            <TextControl
                                                type="text"
                                                value={personName}
                                                placeholder={__('Person Name', 'structured-content')}
                                                className="wp-block-structured-content-person__personName"
                                                onChange={(personName) => setAttributes({personName: personName})}
                                            />
                                        </div>
                                        <div className="sc-input-group" style={{marginTop: 15}}>
                                            <div className="sc-input-label">
                                                {__('Job Title', 'structured-content')}
                                            </div>
                                            <TextControl
                                                type="text"
                                                value={jobTitle}
                                                placeholder={__('Professor', 'structured-content')}
                                                className="wp-block-structured-content-person__jobTitle"
                                                onChange={(jobTitle) => setAttributes({jobTitle: jobTitle})}
                                            />
                                        </div>
                                    </div>
                                    <div className="sc-person-image">
                                        <div className="sc-input-group">
                                            <div className="sc-input-label">
                                                {__('Image', 'structured-content')}
                                            </div>
                                            <div>
                                                <MediaUpload
                                                    onSelect={onImageSelect}
                                                    type="image"
                                                    value={imageID}
                                                    render={({open}) => (
                                                        <button className="components-button is-button is-default"
                                                                onClick={open}>
                                                            {__('Add Image', 'structured-content')}
                                                        </button>
                                                    )}
                                                />
                                                {thumbnailImageUrl && (
                                                    <div>
                                                        <div style={{
                                                            'height'      : 'auto',
                                                            'marginTop'   : 15,
                                                            'background'  : 'rgb(255, 255, 255)',
                                                            'boxShadow'   : 'rgba(0, 0, 0, 0.16) 0px 2px 2px, rgba(0, 0, 0, 0.08) 0px 0px 1px',
                                                            'borderRadius': '4px',
                                                            'maxWidth'    : '100%',
                                                            'width'       : 'auto',
                                                            'overflow'    : 'hidden',
                                                            'marginBottom': 10
                                                        }}>
                                                            <img src={thumbnailImageUrl} style={{
                                                                'width' : 'auto',
                                                                'height': 'auto',
                                                                maxWidth: '100%',
                                                                'margin': 0
                                                            }}/>
                                                        </div>
                                                        <button className="components-button is-button is-default"
                                                                onClick={() => onRemoveImage()}>
                                                            {__('Remove Image', 'structured-content')}
                                                        </button>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Contact', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('E-Mail', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('jane-doe@xyz.edu', 'structured-content')}
                                        value={email}
                                        type="email"
                                        className='wp-block-structured-content-person__email'
                                        onChange={(email) => setAttributes({email: email})}
                                    />
                                </div>
                                <div className="sc-input-group" style={{marginTop: 15}}>
                                    <div className="sc-input-label">
                                        {__('URL', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('http://www.janedoe.com', 'structured-content')}
                                        value={homepage}
                                        type="url"
                                        className='wp-block-structured-content-person__homepage'
                                        onChange={(homepage) => setAttributes({homepage: homepage})}
                                    />
                                </div>
                                <div className="sc-input-group" style={{marginTop: 15}}>
                                    <div className="sc-input-label">
                                        {__('Telephone', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('(425) 123-4567', 'structured-content')}
                                        value={telephone}
                                        type="tel"
                                        className='wp-block-structured-content-person__telephone'
                                        onChange={(telephone) => setAttributes({telephone: telephone})}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="sc-row" style={{marginTop: 15}}>
                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Address', 'structured-content')}
                                </div>
                                <div className="sc-input-group">
                                    <div className="sc-input-label">
                                        {__('Street', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('Any Street 3A', 'structured-content')}
                                        type="text"
                                        value={streetAddress}
                                        className="wp-block-structured-content-person__streetAddress"
                                        onChange={(streetAddress) => setAttributes({streetAddress: streetAddress})}
                                    />
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Postal Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('Any Postal Code', 'structured-content')}
                                            type="text"
                                            value={postalCode}
                                            className="wp-block-structured-content-person__postalCode"
                                            onChange={(postalCode) => setAttributes({postalCode: postalCode})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Locality', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('Any City', 'structured-content')}
                                            type="text"
                                            value={addressLocality}
                                            className="wp-block-structured-content-person__addressLocality"
                                            onChange={(addressLocality) => setAttributes({addressLocality: addressLocality})}
                                        />
                                    </div>
                                </div>
                                <div className="sc-row">
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Country ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('US', 'structured-content')}
                                            type="text"
                                            value={addressCountry}
                                            className="wp-block-structured-content-person__addressCountry"
                                            onChange={(addressCountry) => setAttributes({addressCountry: addressCountry})}
                                        />
                                    </div>
                                    <div className="sc-input-group">
                                        <div className="sc-input-label">
                                            {__('Region ISO Code', 'structured-content')}
                                        </div>
                                        <TextControl
                                            placeholder={__('CA', 'structured-content')}
                                            type="text"
                                            value={addressRegion}
                                            className="wp-block-structured-content-person__addressRegion"
                                            onChange={(addressRegion) => setAttributes({addressRegion: addressRegion})}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="sc-grey-box">
                                <div className="sc-box-label">
                                    {__('Colleague', 'structured-content')}
                                </div>
                                <div className="sc-input-group" style={{marginTop: 15}}>
                                    <div className="sc-input-label">
                                        {__('URL', 'structured-content')}
                                    </div>
                                    <div>
                                        <div>{colleaguesUrls}</div>
                                        <button type="button" style={{
                                            backgroundColor: 'black',
                                            color          : 'white',
                                            borderRadius   : 4,
                                            padding        : 8,
                                            fontSize       : 12,
                                            border         : 'none',
                                            paddingLeft    : 24,
                                            position       : 'relative',
                                            cursor         : 'pointer'
                                        }} onClick={addToColleague}>
                                        <span style={{
                                            display  : 'inline-block',
                                            position : 'absolute',
                                            left     : 8,
                                            top      : '50%',
                                            transform: 'translateY(-50%)',
                                        }}>
                                             <svg
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 fill="none"
                                                 viewBox="0 0 10 10"
                                                 style={{
                                                     width   : 12,
                                                     height  : 12,
                                                     position: 'relative',
                                                     top     : 1,
                                                 }}
                                             >
                                            <circle cx="5" cy="5" r="5" fill="#fff"/>
                                            <path fill="#000" d="M5.7 7.8V5.6h2V4.4h-2V2.2H4.3v2.2h-2v1.2h2v2.2h1.4z"/>
                                        </svg>
                                        </span>
                                            {__('Add One', 'structured-content')}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        ];
    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save: function (props) {

        const {attributes, className} = props;
        const {thumbnailImageUrl} = props.attributes;

        function colleaguesSave() {
            let list = '';
            attributes.colleagues.map(function (value, index) {
                if (value.url !== '')
                    list += `${value.url},`;
            });
            list = list.substr(0, list.length - 1);
            return list;
        }

        return (
            `[sc_fs_person 
            html="${attributes.giveHTML}"
            person_name="${attributes.personName}" 
            job_title="${attributes.jobTitle}" 
            image_id="${attributes.imageID}"
            street_address="${attributes.streetAddress}"
            address_locality="${attributes.addressLocality}"
            address_region="${attributes.addressRegion}"
            postal_code="${attributes.postalCode}"
            address_country="${attributes.addressCountry}"
            email="${attributes.email}"
            url="${attributes.homepage}"
            telephone="${attributes.telephone}"
            css_class="${attributes.cssClass}"
            colleague="${colleaguesSave()}"
            ]`
        );
    }
});
