/**
 * Info Label
 */
function InfoLabel(attributes) {

    const {
        URL,
        float,
    } = attributes;

    const css = {
        "sc_info_label_a": {
            "backgroundColor": "#DAF1E8",
            "borderRadius": "4px",
            "padding": "4px",
            "fontSize": "14px",
            "color": "#114431",
            "lineHeight": "18px",
            "display": "table",
            "position": "relative",
            "paddingLeft": "24px",
            "float": float ? float : 'left'
        },
        "sc_info_label_svg": {
            "width": "16px",
            "height": "16px",
            "display": "inline-block",
            "fill": "#114431",
            "position": "absolute",
            "left": "4px",
            "top": "5px"
        }
    };

    return [
        <a href={URL} target="_blank" style={css.sc_info_label_a}>
            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' style={css.sc_info_label_svg}
                 viewBox='0 0 24 24'>
                <path d='M0 0h24v24H0z' fill='none'/>
                <path
                    d='M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z'
                />
            </svg>
            Info
        </a>
    ];
}

export default InfoLabel;