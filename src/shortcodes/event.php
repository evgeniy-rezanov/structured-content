<?php
/**
 * structuredcontent
 * event.php
 *
 *
 * @category Production
 * @author anl
 * @package  Default
 * @date     2019-06-26 22:10
 * @license  http://structuredcontent.com/license.txt structuredcontent License
 * @version  GIT: 1.0
 * @link     https://structuredcontent.com/
 */ ?>

<?php if ( $atts['html'] === 'true' ) : ?>
    <section class="<?php echo ( empty( $atts['css_class'] ) ) ? 'sc_fs_event sc_card' : $atts['css_class']; ?>">
		<?php
		echo $atts['headline_open_tag'];
		echo esc_attr( $atts['title'] );
		echo $atts['headline_close_tag'];
		?>
        <p>
	        <?php echo htmlspecialchars_decode( do_shortcode( $content ) ); ?>
        </p>
        <div class="sc-row">
            <div class="sc-grey-box">
                <div class="sc-box-label">
					<?php echo __( 'Event Meta', 'structured-content' ) ?>
                </div>
                <div class="sc-company">
                    <div class="sc-company-infos">
                        <div class="sc-input-group">
                            <div class="sc-input-label">
								<?php echo __( 'Name', 'structured-content' ) ?>
                            </div>
                            <div class="wp-block-structured-content-event__location">
								<?php echo $atts['event_location'] ?>
                            </div>
                        </div>
                        <div class="sc-input-group">
                            <div class="sc-input-label">
								<?php echo __( 'Start Date', 'structured-content' ) ?>
                            </div>
                            <div class="wp-block-structured-content-event__sameAs">
								<?php echo date('d.m.Y', strtotime($atts['start_date'])) ?>
                            </div>
                        </div>
                        <div class="sc-input-group">
                            <div class="sc-input-label">
								<?php echo __( 'End Date', 'structured-content' ) ?>
                            </div>
                            <div class="wp-block-structured-content-event__sameAs">
								<?php echo date('d.m.Y', strtotime($atts['end_date'])) ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sc-grey-box">
                <div class="sc-box-label">
					<?php echo __( 'EVENT Location', 'structured-content' ) ?>
                </div>
                <div class="sc-input-group">
                    <div class="sc-input-label">
						<?php echo __( 'Street', 'structured-content' ) ?>
                    </div>
                    <div class="wp-block-structured-content-event__streetAddress">
						<?php echo $atts['street_address'] ?>
                    </div>
                </div>
                <div class="sc-row">
                    <div class="sc-input-group">
                        <div class="sc-input-label">
							<?php echo __( 'Postal Code', 'structured-content' ) ?>
                        </div>
                        <div class="wp-block-structured-content-event__postalCode">
							<?php echo $atts['postal_code'] ?>
                        </div>
                    </div>
                    <div class="sc-input-group">
                        <div class="sc-input-label">
							<?php echo __( 'Locality', 'structured-content' ) ?>
                        </div>
                        <div class="wp-block-structured-content-event__addressLocality">
							<?php echo $atts['address_locality'] ?>
                        </div>
                    </div>
                </div>
                <div class="sc-row">
                    <div class="sc-input-group">
                        <div class="sc-input-label">
							<?php echo __( 'Country ISO Code', 'structured-content' ) ?>
                        </div>
                        <div class="wp-block-structured-content-event__addressCountry">
							<?php echo $atts['address_country'] ?>
                        </div>
                    </div>
                    <div class="sc-input-group">
                        <div class="sc-input-label">
							<?php echo __( 'Region ISO Code', 'structured-content' ) ?>
                        </div>
                        <div class="wp-block-structured-content-event__addressRegion">
							<?php echo $atts['address_region'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Event",
        "name": "<?php echo $atts['title'] ?>",
        "startDate": "<?php echo $atts['start_date'] ?>",
        "endDate": "<?php echo $atts['end_date'] ?>",
        "location": {
            "@type": "Place",
            "name": "<?php echo $atts['event_location'] ?>",
            "address": {
                "@type": "PostalAddress",
                "streetAddress" : "<?php echo $atts['street_address'] ?>",
				"addressLocality" : "<?php echo $atts['address_locality'] ?>",
				"postalCode" : "<?php echo $atts['postal_code'] ?>",
				"addressRegion" : "<?php echo $atts['address_region'] ?>",
				"addressCountry": "<?php echo $atts['address_country'] ?>"
            }
        },
        "description": "<?php echo str_replace('"','\"', $content); ?>"
    }
</script>
