��    _                      	  	        "     *     2     ;     K     Y  	   \     f  	   t     ~     �  
   �     �     �     �     �     �     �     �     	     
	     $	     @	  #   ]	  $   �	     �	  "   �	     �	     �	     �	  
   
     
  	   
     %
     D
     K
     Q
     `
     r
     y
     }
     �
  	   �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
  h   �
     V  	   b     l     y     �     �     �      �  
   �     �     �  	   �  	   �  	                                 +  	   1     ;     B     I     d  
   j     u     �  +   �  3   �  $   �          7  	   H  
   R     ]     j     x  �  �     J     Y     j     v     ~     �     �     �  
   �     �     �     �     �     �     �                     $     6  $   I     n  (   �     �  !   �  ,   �  )        ;  "   R     u     �     �  
   �     �     �     �  
   �     �            
   (     3  
   7     B     N     \     `  	   e     o     t     {  	   �     �     �     �  J   �     �  	     
        !     3     B     P  %   W     }     �     �     �  	   �  	   �     �     �     �     �     �     �  	   �     �  	             *     8  	   D     N  *   d  3   �  $   �     �     �               %     4     =     ,          2                     /           -   $   [                              %         4                 >   D   1      R      K          5       J       F   8   I   T   G   :           7      !   C   
   '   H   Q   &   W   .   @               \      =   L   9   Z   ?      U       N      M           A   ^   ;                V   (   O   Y   P   E   0       _   *             X   <                   ]       S   6              "   +   #      B   	   )       3          (425) 123-4567 Add Image Add One Address Any City Any Postal Code Any Street 3A CA CSS class Cheating huh? Colleague Company Contact Contractor Country ISO Code Currency ISO Code Daily E-Mail EVENT Location Employment Type Enable or disable blocks End Date Enter Your Event Title... Enter Your Question here... Enter Your job title here... Enter an image description here ... Enter your Event Description here... Enter your answer here... Enter your job description here... Event Event Location Event Location Name Event Meta FAQ Full Time Gordon Böhme, Antonio Leutsch Hourly Image Image Settings Image description Intern Job Job Location Job Meta Job Title Locality Logo Monthly Name Other Part Time Per Diem Person Person Name Personal Pimp your content with some feature boxes, which labels the output with micro formats http://schema.org/ Postal Code Professor Question tag Region ISO Code Remove Image Render HTML Salary Same as (Website / Social Media) Start Date Street Structured Content Telephone Temporary Title tag URL US USD Unit Valid Through Value Volunteer Weekly Yearly additional css classes ... event faq answer faq question http://www.janedoe.com http://www.xyz.edu/students/alicejones.html https://gitlab.com/webwirtschaft/structured-content https://gorbo.de/structured-content/ https://your-website.com jane-doe@xyz.edu job offer job search person offer person search structured-content Project-Id-Version: Structured Content 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/structured-content
POT-Creation-Date: 2019-07-15T08:20:20+00:00
PO-Revision-Date: 2019-07-15 10:30+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Domain: structured-content
Plural-Forms: nplurals=2; plural=(n != 1);
 (030) 12345-67 Bild hinzufügen Hinzufügen Adresse Musterstadt 12345 Beispielstrasse 3A ST CSS Klasse Schummeln gilt nicht!? Mitarbeiter Firma Kontakt Auftragnehmer Land (ISO Code) Währung (ISO Code) Täglich E-Mail Veranstaltungsort Beschäftigungsart Blöcke aktivieren oder deaktivieren Veranstaltungsende Hier Veranstaltungsbezeichnung eingeben. Frage hier eingeben ... Hier Stellenbezeichnung eingeben. Geben Sie hier eine Bildbeschreibung ein.... Hier Veranstaltungsbeschreibung eingeben. Antwort hier eingeben. Hier Stellenbeschreibung eingeben. Veranstaltung Veranstaltungsort Name des Veranstaltungsortes Zusatzinfo FAQ Vollzeit Gordon Böhme, Antonio Leutsch Stündlich Bild hinzufügen Bildeinstellungen Bildbeschreibung Praktikant Job Arbeitsort Job-Details Berufsangaben Ort Logo Monatlich Name Andere Teilzeit Tageweise Person Max Mustermann Personenangaben Füge Inhaltsboxen mit JSON-LD-Mikrodatenausgabe gemäß schema.org hinzu. Postleitzahl Professor Fragen-Tag Region (ISO Code) Bild entfernen HTML ausgeben Gehalt Same as (URL d. Website/Social Media) Veranstaltungsbeginn Straße Strukturierte Inhalte Telefon Befristet Titel tag URL DE EUR Einheit Gültig bis Wert Volontär Wöchentlich Jährlich Zusätzliche CSS-Klasse. Veranstaltung FAQ Antwort FAQ Frage http://www.website.de http://uni.de/mitarbeiter/otto-normal.html https://gitlab.com/webwirtschaft/structured-content https://gorbo.de/structured-content/ https://website.de max@mustermann.de Stellenangebot Jobsuche Stellenangebot Jobsuche structured content 