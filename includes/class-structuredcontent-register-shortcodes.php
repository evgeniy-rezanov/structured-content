<?php
/**
 * structured-content
 * class-structuredcontent-shortcodes.php
 *
 *
 * @category Production
 * @author anl
 * @package  Default
 * @date     2019-05-27 01:17
 * @license  http://structured-content.com/license.txt structured-content License
 * @version  GIT: 1.0
 * @link     https://structured-content.com/
 */


// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Load general assets for our blocks.
 *
 * @since 1.0.0
 */
class StructuredContent_Shortcodes {


	/**
	 * This plugin's instance.
	 *
	 * @var StructuredContent_Shortcodes
	 */
	private static $instance;

	/**
	 * Registers the plugin.
	 */
	public static function register() {
		if ( null === self::$instance ) {
			self::$instance = new StructuredContent_Shortcodes();
		}
	}

	/**
	 * The base URL path (without trailing slash).
	 *
	 * @var string $_url
	 */
	private $_url;

	/**
	 * The plugin version.
	 *
	 * @var string $_version
	 */
	private $_version;

	/**
	 * The plugin version.
	 *
	 * @var string $_slug
	 */
	private $_slug;

	/**
	 * The Constructor.
	 */
	private function __construct() {
		$this->_version = STRUCTURED_CONTENT_VERSION;
		$this->_slug    = 'structured-content';
		$this->_url     = untrailingslashit( plugins_url( '/', dirname( __FILE__ ) ) );

		add_shortcode( 'sc_fs_faq', array( $this, 'faq' ) );
		add_shortcode( 'sc_fs_job', array( $this, 'job' ) );
		add_shortcode( 'sc_fs_event', array( $this, 'event' ) );
		add_shortcode( 'sc_fs_person', array( $this, 'person' ) );

	}

	public function faq( $atts, $content = null ) {

		$merged_atts = shortcode_atts(
			array(
				'css_class' => '',
				'headline'  => 'h2',
				'img'       => 0,
				'img_alt'   => '',
				'question'  => '',
				'answer'    => '',
				'html'      => 'true',
			), $atts );

		if ( ! empty( $merged_atts['img'] ) ) {
			$image_id       = intval( $merged_atts['img'] );
			$image_url      = wp_get_attachment_url( $image_id );
			$image_thumburl = wp_get_attachment_image_url( $image_id, array( 150, 150 ) );
			$image_meta     = wp_get_attachment_metadata( $image_id );
			//var_dump($image_meta);
			if ( $image_thumburl !== false && $image_meta !== false && $image_url !== false ) {
				$merged_atts['img_url']       = $image_url;
				$merged_atts['thumbnail_url'] = $image_thumburl;
				$merged_atts['img_size']      = array( $image_meta['width'], $image_meta['height'] );
				if ( empty( $merged_atts['img_alt'] ) ) {
					$merged_atts['img_alt'] = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
				}
			} else {
				$merged_atts['img'] = 0;
			}
		}
		$merged_atts['headline_open_tag']  = '<' . $merged_atts["headline"] . '>';
		$merged_atts['headline_close_tag'] = '</' . $merged_atts["headline"] . '>';

		$atts = $merged_atts;
		ob_start();
		include STRUCTURED_CONTENT_PLUGIN_DIR . 'src/shortcodes/faq.php';
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

	public function job( $atts, $content = null ) {

		$merged_atts = shortcode_atts(
			array(
				'css_class'          => '',
				'title_tag'          => 'h2',
				'html'               => 'true',
				'title'              => '',
				'description'        => '',
				'valid_through'      => '',
				'employment_type'    => '',
				'company_name'       => '',
				'same_as'            => '',
				'logo_id'            => '',
				'street_address'     => '',
				'address_locality'   => '',
				'address_region'     => '',
				'postal_code'        => '',
				'address_country'    => '',
				'currency_code'      => '',
				'quantitative_value' => '',
				'base_salary'        => '',
			), $atts );

		if ( ! empty( $merged_atts['logo_id'] ) ) {
			$image_id       = intval( $merged_atts['logo_id'] );
			$image_url      = wp_get_attachment_url( $image_id );
			$image_thumburl = wp_get_attachment_image_url( $image_id, array( 150, 150 ) );
			$image_meta     = wp_get_attachment_metadata( $image_id );
			//var_dump($image_meta);
			if ( $image_thumburl !== false && $image_meta !== false && $image_url !== false ) {
				$merged_atts['logo_url']      = $image_url;
				$merged_atts['thumbnail_url'] = $image_thumburl;
				$merged_atts['logo_size']     = array( $image_meta['width'], $image_meta['height'] );
				if ( empty( $merged_atts['logo_alt'] ) ) {
					$merged_atts['logo_alt'] = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
				}
			} else {
				$merged_atts['logo'] = 0;
			}
		}
		$merged_atts['headline_open_tag']  = '<' . $merged_atts["title_tag"] . '>';
		$merged_atts['headline_close_tag'] = '</' . $merged_atts["title_tag"] . '>';

		$atts = $merged_atts;

		ob_start();
		include STRUCTURED_CONTENT_PLUGIN_DIR . 'src/shortcodes/job.php';
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

	public function event( $atts, $content = null ) {

		$merged_atts = shortcode_atts(
			array(
				'css_class'        => '',
				'title_tag'        => 'h2',
				'html'             => 'true',
				'title'            => '',
				'description'      => '',
				'start_date'       => '',
				'end_date'         => '',
				'event_location'   => '',
				'street_address'   => '',
				'address_locality' => '',
				'address_region'   => '',
				'postal_code'      => '',
				'address_country'  => '',
				'currency_code'    => '',
			), $atts );

		$merged_atts['headline_open_tag']  = '<' . $merged_atts["title_tag"] . '>';
		$merged_atts['headline_close_tag'] = '</' . $merged_atts["title_tag"] . '>';

		$atts = $merged_atts;
		ob_start();
		include STRUCTURED_CONTENT_PLUGIN_DIR . 'src/shortcodes/event.php';
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

	public function person( $atts, $content = null ) {

		$merged_atts = shortcode_atts(
			array(
				'css_class'        => '',
				'html'             => 'true',
				'person_name'      => '',
				'job_title'        => '',
				'image_id'         => '',
				'email'            => '',
				'telephone'        => '',
				'url'              => '',
				'colleague'        => '',
				'street_address'   => '',
				'address_locality' => '',
				'address_region'   => '',
				'postal_code'      => '',
				'address_country'  => '',
			), $atts );

		if ( ! empty( $merged_atts['image_id'] ) ) {
			$image_id       = intval( $merged_atts['image_id'] );
			$image_url      = wp_get_attachment_url( $image_id );
			$image_thumburl = wp_get_attachment_image_url( $image_id, array( 150, 150 ) );
			$image_meta     = wp_get_attachment_metadata( $image_id );

			if ( $image_thumburl !== false && $image_meta !== false && $image_url !== false ) {
				$merged_atts['image_url']     = $image_url;
				$merged_atts['thumbnail_url'] = $image_thumburl;
				$merged_atts['image_size']    = array( $image_meta['width'], $image_meta['height'] );
				if ( empty( $merged_atts['image_alt'] ) ) {
					$merged_atts['image_alt'] = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
				}
			} else {
				$merged_atts['image'] = 0;
			}
		}

		$merged_atts['links'] = explode( ",", $atts['colleague'] );

		$atts = $merged_atts;

		ob_start();
		include STRUCTURED_CONTENT_PLUGIN_DIR . 'src/shortcodes/person.php';
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

}

StructuredContent_Shortcodes::register();
